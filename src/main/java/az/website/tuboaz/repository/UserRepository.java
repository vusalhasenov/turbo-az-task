package az.website.tuboaz.repository;

import az.website.tuboaz.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User,Long> {
}
