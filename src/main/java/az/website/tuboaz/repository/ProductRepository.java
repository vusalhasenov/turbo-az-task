package az.website.tuboaz.repository;

import az.website.tuboaz.entity.Product;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;


public interface ProductRepository extends JpaRepository<Product,Long> {


    Optional<Product> findByMark(String mark);

    Optional<Product>  findByMarkAndModel(String mark,String model);
}
