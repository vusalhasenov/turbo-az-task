package az.website.tuboaz.repository;

import az.website.tuboaz.entity.Product;
import az.website.tuboaz.entity.ProductInfo;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;


public interface ProductInfoRepository extends JpaRepository<ProductInfo,Long> {



}
