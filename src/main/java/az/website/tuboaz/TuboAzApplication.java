package az.website.tuboaz;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TuboAzApplication {

    public static void main(String[] args) {
        SpringApplication.run(TuboAzApplication.class, args);
    }

}
