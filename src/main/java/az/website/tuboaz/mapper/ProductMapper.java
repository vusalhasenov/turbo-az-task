package az.website.tuboaz.mapper;

import az.website.tuboaz.dto.ProductDto;
import az.website.tuboaz.entity.Product;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel = "spring")
public interface ProductMapper {

    ProductDto mapToDto(Product product);

    Product mapToEntity(ProductDto productDto);
}
