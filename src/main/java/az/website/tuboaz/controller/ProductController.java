package az.website.tuboaz.controller;


import az.website.tuboaz.dto.ProductDto;
import az.website.tuboaz.service.ProductService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("api/product")
public class ProductController {

    private final ProductService productService;

    public ProductController(ProductService productService) {
        this.productService = productService;
    }

    @GetMapping
    public ResponseEntity<List<ProductDto>> findAll(){
        return ResponseEntity.ok(productService.findAll());
    }

    @GetMapping("/findby-mark/{mark}")
    public ResponseEntity<ProductDto> findByMark(@PathVariable String mark){
       return ResponseEntity.ok(productService.findByMark(mark));
    }

    @GetMapping("/findby-mark-model/{model}/{mark}")
    public ResponseEntity<ProductDto> findByMarkAndModel(@PathVariable String mark, @PathVariable String model){
        return ResponseEntity.ok(productService.findByMarkAndModel(mark,model));
    }

    @PostMapping
    public String create(@RequestBody ProductDto productDto){
        productService.create(productDto);
        return "ok";
    }

    @PutMapping("/{id}")
    public ResponseEntity<ProductDto> update(@RequestBody ProductDto productDto,Long id){
        return ResponseEntity.ok(productService.update(id,productDto));
    }

    @DeleteMapping("/{id}")
    public String delete(Long id){
        productService.delete(id);
        return "delete successfully";
    }
}
