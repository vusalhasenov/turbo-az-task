package az.website.tuboaz.service.impl;


import az.website.tuboaz.dto.ProductDto;
import az.website.tuboaz.entity.Product;
import az.website.tuboaz.entity.User;
import az.website.tuboaz.enums.SituationType;
import az.website.tuboaz.mapper.ProductMapper;
import az.website.tuboaz.repository.CategoryRepository;
import az.website.tuboaz.repository.ProductInfoRepository;
import az.website.tuboaz.repository.ProductRepository;
import az.website.tuboaz.repository.UserRepository;
import az.website.tuboaz.service.ProductService;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class ProductServiceImpl implements ProductService {

    private final ProductRepository productRepository;
    private final ModelMapper mapper;

    @Override
    public ProductDto findByMark(String mark) {
        return productRepository.findByMark(mark)
                .map(product -> mapper.map(product, ProductDto.class))
                .orElseThrow(() -> new NullPointerException("product not found"));
    }

    @Override
    public List<ProductDto> findAll() {
        return productRepository.findAll()
                .stream()
                .map(product -> {
                    ProductDto productDto = mapper.map(product, ProductDto.class);
                    productDto.setSituation(SituationType.fromValue(product.getSituationType().getValue()).name());
                    return productDto;
                })
                .collect(Collectors.toList());
    }

    @Override
    public ProductDto findByMarkAndModel(String mark,String model) {
        return productRepository.findByMarkAndModel(mark,model)
                .map(product -> mapper.map(product,ProductDto.class))
                .orElseThrow(()-> new NullPointerException("product Not found"));
    }

    @Override
    @Transactional
    public void create(ProductDto productDto) {
         Product product = mapper.map(productDto,Product.class);
         productRepository.save(product);
    }

    @Override
    public ProductDto update(Long id, ProductDto productDto) {
        productRepository.findById(id).orElseThrow(() -> new NullPointerException("product not found"));
        Product product = mapper.map(productDto,Product.class);
        product.setId(id);
        productRepository.save(product);
        return mapper.map(product,ProductDto.class);
    }

    @Override
    public void delete(Long id) {
        productRepository.findById(id).orElseThrow(()->new NullPointerException("product not found"));
        productRepository.deleteById(id);
    }
}
