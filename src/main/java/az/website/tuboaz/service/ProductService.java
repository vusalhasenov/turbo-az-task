package az.website.tuboaz.service;


import az.website.tuboaz.dto.ProductDto;

import java.util.List;

public interface ProductService {

    ProductDto findByMark(String mark);

    List<ProductDto> findAll();

    ProductDto findByMarkAndModel(String mark,String model);

    void create(ProductDto productDto);

    ProductDto update(Long id ,ProductDto productDto);

    void delete(Long id);
}
