package az.website.tuboaz.dto;

import az.website.tuboaz.entity.Product;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Table;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;

@Data
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level= AccessLevel.PRIVATE)
public class ProductInfoDto {

    Long id;
    Long graduationYear;
    String banType;
    String colour;
    String engine;
    String march;
    String gearBox;
    Long numberOfSeats;
    Long numberOfViews;
    String isNew;
    String gear;
    Integer status;


}
