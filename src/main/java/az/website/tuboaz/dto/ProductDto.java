package az.website.tuboaz.dto;


import az.website.tuboaz.enums.SituationType;
import com.fasterxml.jackson.annotation.JsonFormat;
import jakarta.persistence.CascadeType;
import jakarta.persistence.FetchType;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToOne;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.FieldDefaults;

import java.time.LocalDate;


@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
@FieldDefaults(level = AccessLevel.PRIVATE)
public class ProductDto {
    Long id;
    String model;
    String mark;
    @JsonFormat(pattern = "dd.MM.yyyy")
    LocalDate createAt;
    @JsonFormat(pattern = "dd.MM.yyyy")
    LocalDate updateAt;
    Long price;
    Integer owners;
    String situation;
    String market;
    Integer status;
    UserDto user;
    CategoryDto category;
    ProductInfoDto productInfo;
    String description;

}
