package az.website.tuboaz.entity;

import az.website.tuboaz.enums.SituationType;
import jakarta.persistence.CascadeType;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Table;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;

import java.time.LocalDate;
@Entity
@Table(name="product")
@Data
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level= AccessLevel.PRIVATE)
public class Product {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    Long id;
    String model;
    String mark;
    LocalDate createAt;
    LocalDate updateAt;
    Long price;
    Integer owners;
    SituationType situationType;
    String market;
    Integer status;
    @ManyToOne(cascade= CascadeType.ALL,fetch= FetchType.LAZY)
    User user;
    @ManyToOne(cascade=CascadeType.ALL,fetch=FetchType.LAZY)
    Category category;
    @OneToOne(cascade=CascadeType.ALL)
    @JoinColumn(name = "product_info_id", referencedColumnName = "id")
    ProductInfo productInfo;
    String description;
}
