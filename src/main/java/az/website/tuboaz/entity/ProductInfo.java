package az.website.tuboaz.entity;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Table;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;

@Entity
@Table(name="product_info")
@Data
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level= AccessLevel.PRIVATE)
public class ProductInfo {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    Long id;
    Long graduationYear;
    String banType;
    String colour;
    String engine;
    String march;
    String gearBox;
    Long numberOfSeats;
    Long numberOfViews;
    String isNew;
    String gear;
    Integer status;
    @OneToOne(mappedBy = "productInfo",cascade=CascadeType.ALL)
    Product product;

}
